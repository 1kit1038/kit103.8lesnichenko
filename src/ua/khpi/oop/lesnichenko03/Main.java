package ua.khpi.oop.lesnichenko03;
import javax.swing.*;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
/*
* Ввести текст. Знайти та вивести, скільки разів повторюється в тексті кожне слово.
* Результат вивести у вигляді таблиці.*/
public class Main {
    /**
     * Function that calculates frequency of words and roll outs
    */
    private static void calculating(Vector<String> example)
    {
        Map<String, Integer> map = new HashMap<>(); // Create HashMap for adding words as array
        int counter; // Counter frequency
        for ( int i = 0; i < example.size(); i++) { //1-st loop for finding words in Vector
            counter = 0;
            for (String ss : example) { // 2-nd loop for checking words
                if (example.get(i).equals(ss)) {
                    counter++;
                }
            }
            Integer frequency = counter;
        map.put(example.get(i) , frequency); // Adding words
        map.remove(example.get(i), 1); // Remove words that replays only 1 time
        }

        System.out.println();

        System.out.print("Result(amount frequency of words in the text:\n");
        System.out.print("  Words:  |  Frequency:\n");
        for (Map.Entry<String, Integer> entry : map.entrySet())
        {
            String key = entry.getKey();
            Integer freq = entry.getValue();
            System.out.print(key + "\t\t\t\t");
            System.out.println(freq);
        }
    }
    /**
     * Function that finds and adds words in Vector ( via spaces )*/
    private static void searching(String value)
    {
        char space = ' ';
        int index = 0, j = 0; // index - index of loop where is last space is , j - helping counter for skipping comas, dots, spaces
        Vector<String> out = new Vector<>();
        for(int i=0; i < value.length(); i++)
        {
            if(value.charAt(i) == space || value.charAt(i) == '.'||value.charAt(i) == ',') { // Checking
                out.add(value.substring(index, i)); // Adding words from string in Vector
                if(value.charAt(i) == '.'||value.charAt(i) == ','){
                index = i+1;i++;}else {
                index = i;}
                j++;
            }
        }
        calculating(out); // Call function to calculate
    }

    public static void main(String []args) {
        String textfile = "resources//textfortask03"; // Reading file from PC
        try (BufferedReader reading = new BufferedReader(new FileReader(textfile))) { // Using try-catch with myself 'catch'
            System.out.println("......File successfully opened......\n");
            StringBuilder sb = new StringBuilder(); // Creating string to get and add text in
            String line = reading.readLine(); // Read text
            while (line != null) { // Loop that read lines until line will null
                sb.append(line);
                sb.append(System.lineSeparator()); // System.out.println() , "\n"
                line = reading.readLine();
            }
            String everything = sb.toString(); // Convert to String from StringBuilder
            System.out.println("Text:\n" + everything);
            searching(everything); // Call function to finding words
        } catch (IOException ex) {
            System.out.println("......Error...... : " + ex.getMessage());
        }
    }
}