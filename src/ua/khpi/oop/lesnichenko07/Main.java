package ua.khpi.oop.lesnichenko07;

import ua.khpi.oop.lesnichenko07.Ticket;

public class Main {
    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        ticket.setNameStation("Valky station - Kharkov station");
        ticket.setTimeArrive("12-03-2020 16:30:00");
        ticket.setTimeMiddle("12-03-2020 23:00:00");
        ticket.setTimeDeparture("13-03-2020 10:25:13");
        ticket.setGeneralPlaces(30);
        ticket.setBoughtTickets(26);
        ticket.setFreePlaces();
        ticket.setNumber(15);

        System.out.println("Number race: " + ticket.getNumber() + "\nName race: " + ticket.getNameStation() +
                "\nTime departure: " + ticket.getTimeDeparture() + "\nLayover time: " + ticket.getTimeMiddle() +
                "\nTime arrive: " + ticket.getTimeArrive() + "\nFree places: " + ticket.getFreePlaces() +
                "\nGeneral amounts of places: " + ticket.getGeneralPlaces());

    }

}
