package ua.khpi.oop.lesnichenko07;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.Serializable;

public class Ticket implements Serializable {
    private String nameStation;
    private String strTimeArrive;
    private String strTimeMiddle;
    private String strTimeDeparture;
    private int freePlaces;
    private int generalPlaces;
    private int boughtTickets;
    private int number;

    public Ticket() {

    }

    public String getNameStation() {
        return nameStation;
    }

    public void setNameStation(String nameStation1) {
        this.nameStation = nameStation1;
    }

    public String getTimeArrive() {
        return strTimeArrive;
    }

    public void setTimeArrive(String timeArrive1) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        try {
            Date timeArrive = dateFormat.parse(timeArrive1);
            this.strTimeArrive = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(timeArrive);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }
    public String getTimeDeparture() {
        return strTimeDeparture;
    }
    public void setBoughtTickets(int BoughtTickets){
        this.boughtTickets = BoughtTickets;
    }

    public void setTimeDeparture(String TimeDeparture1) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        try {
            Date timeDeparture = dateFormat.parse(TimeDeparture1);
            this.strTimeDeparture = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(timeDeparture);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }

    public void setTimeMiddle(String TimeMiddle1) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        try {
            Date timeMiddle = dateFormat.parse(TimeMiddle1);
            this.strTimeMiddle = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(timeMiddle);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }

    public String getTimeMiddle() {
        return strTimeMiddle;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces() {
        this.freePlaces = generalPlaces - boughtTickets;
    }

    public int getGeneralPlaces() {
        return generalPlaces;
    }

    public void setGeneralPlaces(int generalPlaces1) {
        this.generalPlaces = generalPlaces1;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number1) {
        this.number = number1;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
