package ua.khpi.oop.lesnichenko01;

class Main{

   static int count; // Value of counted digits
   static int uncount; // Value of uncounted digits
   static int value; // Value of number 1 in each digit number
    /**
    * Function that convert from 10 to 2-ry*/
   static void binar(int a){

       int b;
       String temp = "";

       while (a != 0){
           b = a % 2;
           temp = b + "." + temp;
           a /= 2;
       if(b == 1) value++;
       else if (b == -1) value++; // Need this one, because conversation from long to int makes some loses
       }

       System.out.println(temp);

    }
/**
 * Function that calculates and finds count and uncounted digits in each value*/
   private static void Calculate(char[] mas) {

        {
            for (int i = 0; i < mas.length; i++) {
                if (mas[i] % 2 == 1) {
                    uncount++;
                } else if (mas[i] % 2 == 0) {
                    count++;
                }
            }
        }
    }
    public static void main(String []args)
    {
        int nbook = 0x0469F; // 18079
        long number = 380933601297L;
        int last_two_digits_number = 0b1100001; // 97
        int last_four_digits_number = 002421; // 1297
        int mod_div = 9;
        int intnumber = (int) number; // convert from long to int

        String snbook = "" + nbook; // Initialize values into string
        char ss[] = snbook.toCharArray(); // Making string to char due to index of array
        String snumber = "" +number;
        char sn[] = snumber.toCharArray();
        String slasttwo = "" + last_two_digits_number;
        char lt[]= slasttwo.toCharArray();
        String slastfour = "" + last_four_digits_number;
        char lf[] = slastfour.toCharArray();
        String smod = "" + mod_div;
        char sm[] = smod.toCharArray();

        Calculate(ss); // Calls function
        Calculate(sn);
        Calculate(lt);
        Calculate(lf);
        Calculate(sm);

        binar(nbook); // Calls function
        binar(intnumber);
        binar(last_two_digits_number);
        binar(last_two_digits_number);
        binar(mod_div);

        System.out.println(count + "  -------- COUNT");
        System.out.println(uncount + " -------- UNCOUNT");
        System.out.println(value + " -------- VALUE OF 1s");
    }
}