package ua.khpi.oop.lesnichenko06;
import ua.khpi.oop.lesnichenko05.OwnContainer;

import java.io.*;

public class Main extends ua.khpi.oop.lesnichenko03.Main {
    public static void main(String[] args) {
        OwnContainer<String>kop = new OwnContainer<>();

        kop.add("Today");
        kop.add("is");
        kop.add("good day");
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try{
            fos = new FileOutputStream("resources//fileOutPutStream");
            out = new ObjectOutputStream(fos);
            out.writeObject(kop);
            out.close();
        }catch (Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        OwnContainer<String>neKop = null;
        try {
            fis = new FileInputStream("resources//fileOutPutStream");
            ois = new ObjectInputStream(fis);
            neKop = (OwnContainer<String>)ois.readObject();
            ois.close();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        System.out.println(neKop);
    }
}
