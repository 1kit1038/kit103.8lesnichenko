package ua.khpi.oop.lesnichenko04;
import java.io.*;
import java.util.*;

/*
 * Ввести текст. Знайти та вивести, скільки разів повторюється в тексті кожне слово.
 * Результат вивести у вигляді таблиці.*/
public class Main {
    /**
     * Function that calculates frequency of words and roll outs
     */
    private static void calculating(Vector<String> example)
    {
        Map<String, Integer> map = new HashMap<>(); // Create HashMap for adding words as array
        int counter; // Counter frequency
        for ( int i = 0; i < example.size(); i++) { //1-st loop for finding words in Vector
            counter = 0;
            for (String ss : example) { // 2-nd loop for checking words
                if (example.get(i).equals(ss)) {
                    counter++;
                }
            }
            Integer frequency = counter;
            map.put(example.get(i) , frequency); // Adding words
            map.remove(example.get(i), 1); // Remove words that replays only 1 time
        }

        System.out.println();

        System.out.print("Result(amount frequency of words in the text:\n");
        System.out.print("Words:  |  Frequency:\n");
        for (Map.Entry<String, Integer> entry : map.entrySet())
        {
            String key = entry.getKey();
            Integer freq = entry.getValue();
            System.out.printf("%-19s %-23d\n" , key,freq);
        }
    }
    private static void openingFile(String path)
    {
        try (BufferedReader reading = new BufferedReader(new FileReader(path))) { // Using try-catch with myself 'catch'
            System.out.println("......File successfully opened......\n");
            StringBuilder sb = new StringBuilder(); // Creating string to get and add text in
            String line = reading.readLine(); // Read text
            while (line != null) { // Loop that read lines until line will null
                sb.append(line);
                sb.append(System.lineSeparator()); // System.out.println() , "\n"
                line = reading.readLine();
            }
            String everything = sb.toString(); // Convert to String from StringBuilder
            System.out.println("Text:\n" + everything);
            searching(everything); // Call function to finding words
        } catch (IOException ex) {
            System.out.println("......Error...... : " + ex.getMessage());
        }
    }
    public static void main(String []args) throws IOException {
        System.out.println("Would you like to open file or input your own text?\n Y(Open file)/N(My own text)");
        Scanner ss= new Scanner(System.in);
        String answer = ss.next();
        if(answer.equals("Y") || answer.equals("y")){
            String path = "resources//textfortask03";
            openingFile(path);

        }
        else {
        System.out.println("Write your own text below: ");
            Scanner scanner = new Scanner(System.in);
            String ownText = scanner.next() + scanner.nextLine();
            try(FileWriter fileWriter = new FileWriter("resources//outputtextfortask03",false)) {
                fileWriter.write(ownText);
                System.out.println("Would you like to look up your text?");
                String answerTwo = scanner.next();
                if(answerTwo.equals("Y") || answerTwo.equals("y"))
                {
                    System.out.println(ownText);
                    searching(ownText);
                } else {
                    System.out.println("OK, continuing...");
                    searching(ownText);
                }
                }
            }
        }


    /**
     * Function that finds and adds words in Vector ( via spaces )*/
    private static void searching(String value)
    {
        char space = ' ';
        value += "\n";
        int index = 0;// index - index of loop where is last space is , j - helping counter for skipping comas, dots, spaces
        Vector<String> out = new Vector<>();
        for(int i=0; i < value.length(); i++)
        {
            if(value.charAt(i) == space || value.charAt(i) == '.'||value.charAt(i) == ',' || value.charAt(i) == '\n') { // Checking
                out.add(value.substring(index, i)); // Adding words from string in Vector
                while (value.charAt(i) == '.'||value.charAt(i) == ',' || value.charAt(i) == ' '){
                    i++;}
                index = i;
            }
        }
        calculating(out); // Call function to calculate
    }
}
