package ua.khpi.oop.lesnichenko05;

import java.io.Serializable;
import java.util.Iterator;

public class OwnContainer<E> implements Iterable<String> , Serializable {
    private String[] words;
    private int index = 0;

    public OwnContainer() {
        words = new String[0];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String m : words) {
            if (m != null) {
                sb.append(m).append(" ");
            }
        }
        return sb.toString();
    }

    public void add(String string) {
        if (words.length == 0 || words.length == index) {
            String[] temp = words;
            words = new String[temp.length + 5];
            words[index] = string;
            index++;
            System.arraycopy(temp, 0, words, 0, temp.length);
        } else {
            words[index] = string;
            index++;
        }

    }


    public void clear() {
        words = new String[0];
    }

    public boolean remove(String string) {
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(string)) {
                String[] temp;
                if (index > words.length - 5) {
                    temp = new String[words.length];
                } else {
                    temp = new String[words.length - 5];
                }
                System.arraycopy(words, 0, temp, 0, i);
                System.arraycopy(words, i + 1, temp, i, index - i - 1);
                words = temp;
                index--;

                return true;
            }
        }
        return false;
    }


    public Object[] toArray() {
        return this.words;
    }


    public int size() {
        return index;
    }


    public boolean contains(String string) {
        for (int i = 0; i < index; i++) {
            if (words[i].contains(string))
                return true;
        }
        return false;
    }


    public boolean containsAll(OwnContainer<String> container) {
        boolean res = true;
        String s = this.toString();
        for (String p : container) {
            res &= s.contains(p);
        }
        return res;
    }

    private int iter = 0;

    public Iterator<String> iterator() {
        return new Iterator<String>() {
            @Override
            public boolean hasNext() {
                return iter < index;
            }

            @Override
            public String next() {
                return words[iter++];
            }

            @Override
            public void remove() {
                iter = 0;
            }
        };
    }
}
