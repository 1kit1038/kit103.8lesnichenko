  package ua.khpi.oop.lesnichenko02;
  import java.util.Random;

class Main{
    static int count; // the value of count numbers
    static int uncounted; // the value of uncount numbers
    static int h;

/*
* Main has 1 function */
    public static void main(String[] args)
    {
        int k = 0;
        generate(k); // Calling function that generates a random number
        String spaces = "          "; // 10 spaces
        System.out.println("Number :" + spaces + "Count :"+spaces + "Uncounted :");
        System.out.printf("%d%17d%21d\n" , h,  count, uncounted);
    }

    private static void generate(int a)
    {   Random ran = new Random();
        h = ran.nextInt(90000000) + 9999999; // Number is randomed
        searching(h); // Calling function that searches count and uncounted numbers
    }
    /*
    * Function does searching count and uncounted number
    *
    * */
    private static void searching(int a)
    {
        int buf;
        for(int i=0;i < 8; i++)
        {
           buf = a % 10;

           if(a % 2 == 0)
           {
               count++;
           }else uncounted++;
           a /= 10;
        }

    }
}