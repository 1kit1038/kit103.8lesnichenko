# WebACS



## Development with

- [Angular-cli](https://github.com/angular/angular-cli)  - framework

- [Keycloak](https://www.keycloak.org/) - identity and access management solution

- [MySQL](https://www.mysql.com/) - database

- [Docker](https://www.docker.com/) - PaaS

- [npm](https://www.npmjs.com/) - Node Package Manager

## Installation 

**Install Docker** 

`apt-get install docker` 

**Install docker-compose** 

`apt-get install docker-compose` 

**Install package manager** 

`apt-get install npm` 

**Download config file for docker-compose** 

`wget "https://drive.google.com/uc?export=download&id=1fZDFbNiP06ZTEMBHmlQ8IkR5ahDXYOxb" -O keycloak-mysql.yml`

## Configure keycloak

1. Go to _**Clients**_ 
2. Choose _**master-realm**_
3. In field **_Access Type_** choose **public**
4. In field **_Valid Redirect URIs_** you should write base url of valid redirects url ( in our case - http://localhost:4200/* )
5. In field **_Base URL_** you should write adress of browser app ( in our case - http://localhost:4200/ )
5. In field **_Web Origins_**  put *****
6. Save changes
## Running application

**Check dependiens in app**

`npm install` 

**Run app**

`npm run start`

## Running keycloak server

`docker-compose -f keycloak-mysql.yml up`

## In futures

### Config
1. Access opening the via password ( set in config )
2. Ability changing name of the door ( option in config )

### Cards
1. Output users' images in table ( if have; if not so change by symbols : first name, last name )
2. Output email address ( if have )
3. Output full information about logged in users into system ( first name, last name, email, cardID, curdate )

### Topics

Adding user's topic ( ability setting password, change first name ,last name, day of birth )

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Authors
+ Mihail Kyidin - CEO
+ Andrey Maluga - Coder
+ Maxim Pisnevskiy - Designer
+ Dmitriy Sokolov - Tester
+ Vladislav Lesnichenko - Product manager
## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
